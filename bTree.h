using namespace std;
#include "bTreeNode.h"
#include <stddef.h>
#include <string>
#include <math.h>
#include <iostream>
#include <sstream>
class bTree
{
	private:
		//always keeping track of where the root node becuase this is where to start an insertion in a btree
		bTreeNode *root;
		//calculated min number of keys any node should have before combining nodes or splitting a node
		int mindeg;

		//private wrapper to call bTreeNode::gatherstring() if the tree is not empty
		void gatherstring(stringstream*);

		//private wrapper to call bTreeNode::findNode() if the tree is not empty
		bTreeNode* findNode(string,string*);

		//private function to insert a formed entry
		bool insertentry(entry);

		//just a wrapper for the removalStarter method in the bTreeNode class
		//need this to solve a few issues involving deleting every node in the tree and trying to delete entries from and empty tree! crazy, I know
		bool removalStarter(entry);

	public:
		//constructor
		bTree(double);

		//destructor
		~bTree();

		//public function converts the stringstream to a string
		string toStr();

		//public function to search for a key and put the corresponding value at the address that was passed to it
		bool find(string,string*);

		//public function to insert a new entry consisting of two strings, key and value
		bool insert(string,string);

		//calls removalStarter (the first method to call to remove an entry) with a formed entry from the provided key
		bool delete_key(string);

};
