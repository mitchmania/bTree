#include"bTree.h"
#include <iostream>
using namespace std;

//private wrapper to call bTreeNode::gatherstring() if the tree is not empty
void bTree::gatherstring(stringstream* ss){
	//if the tree is not empty
	if(root != NULL){
		root->gatherstring(ss);
	}
}

//private wrapper to call bTreeNode::findNode() if the tree is not empty
bTreeNode* bTree::findNode(string key,string* valueaddr){
	//if the tree is empty
	if(root == NULL){
		return NULL;
	}else{
		//call findnode starting with the root
		bTreeNode* temp=root->findNode(key,valueaddr);
		return temp;
	}
}

//private function to insert a formed entry
bool bTree::insertentry(entry tempentry){
	//default return value
	bool returnval=false;
    //if theres no root the tree is empty, form a tree
    if (root == NULL){
        //create the root node (root is a ptr to a bTreeNode)
    	//pass it the default parameters, mindeg and "isleaf"=true
        root = new bTreeNode(mindeg, true);
        root->entries[0] = tempentry;  // Insert key
        returnval=true;
        root->entrycount = 1;  // Update number of keys in root

    }else{
        //calculate if the root is full
        if (root->entrycount == 2*mindeg-1){
            //create new root node
        	//default params for mindeg and "isleaf"=false
            bTreeNode *newnode = new bTreeNode(mindeg, false);

            //root is now a child for the newnode
            newnode->children[0] = root;

            //call splitnode on the old root to determine the median create two children
            newnode->splitnode(0, root);

            //always insert entries to nodes at the leaf nodes so determine the proper child to insert
            int a = 0;
            //this if statement is determining which side of the median the new entry will be inserted,  entries[0] is the only entry in the newnode
            if (newnode->entries[0].key < tempentry.key){
                //add one to a essentially making the right child the insertion target
            	a++;
            }
            //readyforinsert is only called on nodes we know have available spaces, we just made this child so we know it has available spaces
            returnval = newnode->children[a]->readyforinsert(tempentry);

            //set the root to the new node/root we made
            root = newnode;
        }
        else{
        	//the root has available space so lets call readyforinsert
            returnval = root->readyforinsert(tempentry);
        }
    }
    return returnval;
}

//just a wrapper for the removalStarter method in the bTreeNode class
//need this to solve a few issues involving deleting every node in the tree and trying to delete entries from and empty tree! crazy, I know
bool bTree::removalStarter(entry tempentry){
    //default return value
	bool returnval=false;
	//if this is an empty tree, we've failed
	if (root==NULL){
        return false;
    }

    //call removalStarter from the root node
	//notice this is the method in the bTreeNode class because root is of type bTreeNode
    returnval = root->removalStarter(tempentry);
    //did we just delete the last entry in the root?? that's bad
    if (root->entrycount==0){
    	//stash this address where the old (now empty) root is so that we can delete it later and avoid a memory leak!
        bTreeNode *stash = root;
        if (root->isleaf){
            //the root was the last node in the tree, tree is now empty, make root NULL again so that we know this when we try to add entries
        	root = NULL;
        }else{
        	//first child of the root node is now root, old root is useless now
            root = root->children[0];
        }
        //so lets delete the old root
        delete stash;
    }
    return returnval;
}

//end private
//begin public

//constructor
bTree::bTree(double mindeginput){
	//empty tree so we dont have a root yet
	root = NULL;
	//calculate min number of entries per node
	mindeg = ceil(mindeginput/2);
}

//destructor
bTree::~bTree(){
	delete root;
}

//public function converts the stringstream to a string
string bTree::toStr(){
	//prints a string by doing in order traversal and each node putting its values in a stringstream recursively
	stringstream ss;
	//call this recursive function
	this->gatherstring(&ss);
	//collect the entire string from the stringstream
	return ss.str();
}

//public function to search for a key and put the corresponding value at the address that was passed to it
bool bTree::find(string key,string *valueaddr){
	//findNode returns a pointer to a node if the key has been found and the value updated
	bTreeNode* temp;
	temp = findNode(key,valueaddr);
	//if temp now points to a bTreeNode, the key was found and the value updated
	if(temp!=NULL){
		return true;
	}else{
		//temp points to a NULL ptr, the was not found, value was not updated
		return false;
	}
}

//public function to insert a new entry consisting of two strings, key and value
bool bTree::insert(string key, string value){
	//builds an entry
	entry tempentry;
	tempentry.key=key;
	tempentry.value=value;
	//call insert entry passing this new entry
	return insertentry(tempentry);
}

//calls removalStarter (the first method to call to remove an entry) with a formed entry from the provided key
bool bTree::delete_key(string key){
	entry tempentry;
	//notice we are only looking for matching keys (not values) so we can form an entry with just this
	tempentry.key=key;
	//return true if a matching entry was located and deleted, otherwise false
	return removalStarter(tempentry);
}









