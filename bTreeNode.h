#include <string>
using namespace std;
//create the struct defining an entry as two strings
//this could be changed to any data type really as long as the key value is comparable with itself
//data types could even be added so that the tree can hold more data! exciting
struct entry{
	string key;
	string value;
};
class bTreeNode
{
	private:
		//array of entries contained in this node
		entry *entries;
		//minimum number of nodes the node is allow to hold before it is split or restructured
		bTreeNode **children;
		//number of entries contained in the node at any given time
		int mindeg;
		//array of pointers to the children nodes of this node
		int entrycount;
		//flag for when the node is a leaf, true if it is a leaf
		bool isleaf;

		//make the bTree class a friend so that it can access the private members
		friend class bTree;

		//constructor
		bTreeNode(int, bool);

		//destructor
		~bTreeNode();

		//insert
		bool readyforinsert(entry);

		//splits a node into two children
		void splitnode(int, bTreeNode*);

		//returns the node containing the value you were seaching for, otherwise returns null ptr
		bTreeNode *findNode(string, string*);

		//recursive helper function that adds keys using inorder traversal to the stringstream, later used to build a string
		void gatherstring(stringstream*);

		//helper function used to find the index of next key, useful for the deletion process
		int findGreaterKey(entry);

		// helper function that is the starting place to remove a node, once the node type is determined, the appropriate removal process is started
		bool removalStarter(entry);

		//removes (or simply overwrites) the key at specified index a
		void leafRemove(int);

		//removes the key at an index a on a inner node
		void innerRemove(int);

		//given the index of an entry in this node, returns the entry from the child to the left of the entry
		entry getLeftChildEntry(int);

		//do the same but reverse, getting the entry one greater
		entry getRightChildEntry(int);

		//"rotates" children if they don't have enough keys to meet the minumum or combines them
		void restructure(int);

		//rotates a key clockwise (moving it up and right)
		void moveRight(int);

		//rotates a key counterclockwise (moving it up and left)
		void moveLeft(int);

		//creates a single child out of two
		void createSingleChild(int);


};
