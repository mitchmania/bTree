#include<iostream>
#include"bTree.h"

#include<string>
using namespace std;
int main()
{
// 	bTree t(3); // A B-Tree with max degree 3
//	t.insert("3","3");
//	t.insert("7","7");
//	t.insert("10","10");
//	t.insert("11","11");
//	t.insert("13","13");
//	t.insert("13","13");
//	t.insert("14","14");
//	t.insert("15","15");
//	t.insert("18","18");
//	t.insert("16","16");
//	t.insert("19","19");
//	t.delete_key("6");
//
//	cout<<t.delete_key("13");
//
//
//	cout<<t.delete_key("7");
//
//
//	cout<<t.delete_key("4");
//
//
//	t.delete_key("2");
//
//	t.delete_key("13");
//	cout<<t.toStr();


	int op = 0;
	int size;
	string key;
	string value;
	cin>>size;

	bTree * BT = new bTree(size);
	while(op < 4) {
		cin >> op;
		switch(op) {
			case 0://Insert
				cin>>key;
				cin>>value;
				cout<<BT->insert(key,value);
				break;
			case 1://Delete
				cin>>key;
				cout<<BT->delete_key(key);
				break;
			case 2://Find
				cin>>key;
				if(BT->find(key,&value))
					cout<<value<<endl;
				else
					cout<<"Key Not Found\n";
				break;
			case 3://toString
				cout<<BT->toStr();
				break;
		}
	}
	return 0;

}
