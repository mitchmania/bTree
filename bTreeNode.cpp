#include "bTreeNode.h"
#include <stddef.h>
#include <iostream>
#include <sstream>
using namespace std;

//constructor
bTreeNode::bTreeNode(int mindeginput, bool leafinput){
	//min number of entries that the node will have
    mindeg = mindeginput;
    //boolean to determine if the
    isleaf = leafinput;

    // Allocate memory for maximum number of possible keys
    // and child pointers
    entries = new entry[2*mindeg-1];
    children = new bTreeNode *[2*mindeg];

    // Initialize the number of keys as 0
    entrycount = 0;
}

//destructor
bTreeNode::~bTreeNode(){
	delete[] entries;
	delete[] children;
}

//insert
bool bTreeNode::readyforinsert(entry newentry){
	//only called if node has an available space for an entry
    //start at the right of the node
    int index = entrycount-1;

    //only want to insert if we are at a leaf
    if (isleaf == true){
        //loop through getting keys greater than and moving them over, making space, moves right to left
        while (index >= 0 && entries[index].key > newentry.key){
        	entries[index+1] = entries[index];
            index--;
        }
        //loop exited so we now have a new space to put a key in
        entries[index+1] = newentry;
        //added an entry so
        entrycount = entrycount+1;
        //successfully added
        return true;
    //not a leaf, so lets get a leaf
    }else{
        //loop through parents entries finding where the appropriate child would be, index is adjusted to be the appropriate value when the loop ends
        while (index >= 0 && entries[index].key > newentry.key){
            index--;
        }
        //count entries in the found child, before we add the entry the child might need to be split
        if (children[index+1]->entrycount == 2*mindeg-1){
            splitnode(index+1, children[index+1]);
            //determine which side of the median the child goes on becuase we just split the node into two
            if (entries[index+1].key < newentry.key)
                index++;
        }
        //recursively call this function for the child we just deemed appropriate for the new key, the child will not be full because we just split it to make room
        return children[index+1]->readyforinsert(newentry);
    }
}

//splits a node into two children
void bTreeNode::splitnode(int split, bTreeNode *nodetosplit){
    //create a new node to move some entries into
    bTreeNode *newnode = new bTreeNode(nodetosplit->mindeg, nodetosplit->isleaf);
    newnode->entrycount = mindeg - 1;

    //copy entries over to new node
    for (int a = 0; a < newnode->entrycount; a++){
    	newnode->entries[a] = nodetosplit->entries[a+mindeg];
    }

    //if this isn't a leaf, we also have to move the appropriate children over to this new node
    if (nodetosplit->isleaf == false)
    {
        for (int a = 0; a < mindeg; a++)
        	newnode->children[a] = nodetosplit->children[a+mindeg];
    }

    //decrease entries
    nodetosplit->entrycount = mindeg - 1;

    //move children over to the right to create a space for new child
    for (int a = entrycount; a >= split+1; a--){
    	children[a+1] = children[a];
    }

    //put the child in the new spot
    children[split+1] = newnode;

    //move the entries over in this new parent to make space for the median
    for (int a = entrycount-1; a >= split; a--){
    	entries[a+1] = entries[a];
    }
    //put the median in the new space
    entries[split] = nodetosplit->entries[mindeg-1];

    //we just added a value so
    entrycount++;
}

//returns the node containing the value you were seaching for, otherwise returns null ptr
bTreeNode *bTreeNode::findNode(string searchingkey,string* valueaddr){
    //loop to find the position that is just greater than k
	int a = 0;
    while (a < entrycount && searchingkey > entries[a].key){
    	a++;
    }
 	//a is now adjusted to be this index
    //look if the key on this entry is equal to the key inputted
    	if (entries[a].key == searchingkey){
    		*valueaddr=entries[a].value;
    		//if it is, we adjust the value at the address that was passed to us (the value pointer)
    		//return this node (just for recursions sake)
    		return this;
    	}
    //did we get all the way down to a leaf and it didn't have the key we were looking for?
    if (isleaf == true)
    	//return null ptr
        return NULL;

    //didn't have the appropriate key we were looking for but it wasn't a leaf so lets go to its greatest child and recursively call this function in it
    return children[a]->findNode(searchingkey,valueaddr);
}

//recursive helper function that adds keys using inorder traversal to the stringstream, later used to build a string
void bTreeNode::gatherstring(stringstream* ss){
	//declare a up here because it is used outside of the loop
	int a;
    for (a = 0; a < entrycount; a++)
    {
        //if this isn't a leaf, we need to start with the least child and traverse it completely, then we get one key from this node and move to traverse the next child completely
        if (isleaf == false){
        	children[a]->gatherstring(ss);
        }
        //send the values to the string stream that is referenced by the pointer
        *ss <<entries[a].key<<endl;
    }
    //grab the last child after we explored all the entries
    if (isleaf == false)
        children[a]->gatherstring(ss);
}

//helper function used to find the index of next key, useful for the deletion process
int bTreeNode::findGreaterKey(entry tempentry){
    int a=0;
    //while the keys are less than the key we are looking for, add one to a
    while (a<entrycount && entries[a].key < tempentry.key)
        a++;
    return a;
}

// helper function that is the starting place to remove a node, once the node type is determined, the appropriate removal process is started
bool bTreeNode::removalStarter(entry entrytoremove){
	//default return value for trying to delete an entry, returns false if the entry is not found, true if found and removed
    bool returnval = false;
    //find the index (applicable for the children array or the entry array) of the location greater than the key value of the entry we wish to remove
	int a = findGreaterKey(entrytoremove);
    //if the key is in this node
    if (a < entrycount && entries[a].key == entrytoremove.key)
    {
    	//the two types of delete, for an inner node and a leaf
        if(isleaf==true){
            leafRemove(a);
        	//we found and deleted the key successfully
            returnval=true;
        }else{
            innerRemove(a);
            //we found and deleted the key successfully
        	returnval=true;
        }
    }
    else
    {
        //if we traversed all the way down to a leaf and the key isn't in it, return false because the key isn't in this tree
        if (isleaf)
        {
            return false;
        }


        //if we are at the end of the array, stash this fact using indicator, a will be changed before we need it
        bool indicator;
        if(a==entrycount){
        	indicator=true;
        }else{
        	indicator= false;
        }

        //after the delete the key might need to be restructured because it has too few entries now
        if (children[a]->entrycount < mindeg){
            restructure(a);
        }
        //if weve looked through all the entries of this node and haven't found what we are looking for, move onto the children to delete from
        if (indicator && a > entrycount)
            returnval = children[a-1]->removalStarter(entrytoremove);
        else
            returnval = children[a]->removalStarter(entrytoremove);
    }
    //returns true if we found the value and deleted it
    return returnval;
}

//removes (or simply overwrites) the key at specified index a
void bTreeNode::leafRemove (int a){
	//easy delete!!! we are at a leaf so just remove the entry and move the values over yay!
    //move values over, overwriting the entry we intend to delete
    for (int b=a+1; b<entrycount;b++)
    	entries[b-1] = entries[b];

    //we deleted an entry so
    entrycount--;
}

//removes the key at an index a on a inner node
void bTreeNode::innerRemove(int a){
	//now things get a bit more complicated
	//both deleting entries here and checking if we need to restructure the tree in this method
	//create a copy of the entry at the specified position
    entry tempentry = entries[a];

    //check if child entry to the left of the entry (less than this entry) is equal to this entry as well, if it is, move in and delete the key,
    //restructuring might be needed after this but that is handled in the removalStarter
    if (children[a]->entrycount >= mindeg){
        entry leftChild = getLeftChildEntry(a);
        entries[a] = leftChild;
        children[a]->removalStarter(leftChild);
    }

    //do the same but to the right of the entry this time
    else if  (children[a+1]->entrycount >= mindeg){
        entry rightChild = getRightChildEntry(a);
        entries[a] = rightChild;
        children[a+1]->removalStarter(rightChild);
    }

    //means that both children are below the min number of entries, we can make one child out of them
    else{
        createSingleChild(a);
        //now we can look at this new child to remove the entry
        children[a]->removalStarter(tempentry);
    }
}

//given the index of an entry in this node, returns the entry from the child to the left of the entry
entry bTreeNode::getLeftChildEntry(int a){
    //to get the child that is one less than our current entry we need to move all the way down to a leaf
    bTreeNode *itemptr=children[a];
    //keep reassigning itemptr until we hit a leaf
    while (itemptr->isleaf==false)
    	itemptr = itemptr->children[itemptr->entrycount];

    //return the rightmost entry of the leaf we found, after all of this, this is the entry that is one less than the entry we started at
    return itemptr->entries[itemptr->entrycount-1];
}

//do the same but reverse, getting the entry one greater
entry bTreeNode::getRightChildEntry(int a){
	//to get the child that is one less than our current entry we need to move all the way down to a leaf
    bTreeNode *itemptr = children[a+1];
    //keep reassigning itemptr until we hit a leaf
    while (!itemptr->isleaf)
    	itemptr = itemptr->children[0];

    //return the leftmost (0) entry of the leaf we found, after all of this, this is the entry that is one greater than the entry we started at
    return itemptr->entries[0];
}

//"rotates" children if they don't have enough keys to meet the minumum or combines them
void bTreeNode::restructure(int a){
    //if left child has enough keys to meet minimum, we can "rotate" a key to the right child
    if(a!=0 && children[a-1]->entrycount>=mindeg){
    	moveRight(a);
    //if the right child has enough keys to meet min we can "rotate" a key to the left child
    }else if(a!=entrycount && children[a+1]->entrycount>=mindeg){
    	moveLeft(a);
    }else{
    	//if not at the end of the entries
        if (a != entrycount)
        	//create single child from the child a and the child to the right of it
            createSingleChild(a);
        else{
        	//create single child from the child a and the child to the left of it
        	createSingleChild(a-1);
        }
    }
}

//rotates a key clockwise (moving it up and right)
void bTreeNode::moveRight(int a){
    bTreeNode *thischild=children[a];
    bTreeNode *leftchild=children[a-1];

    //make space in thischild by moving all entries over
    for (int b=thischild->entrycount-1; b>=0; --b){
    	thischild->entries[b+1] = thischild->entries[b];
    }

    //we will have to move the children over as well unless this is a leaf
    if (thischild->isleaf==false){
        for(int b=thischild->entrycount; b>=0; --b){
        	thischild->children[b+1] = thischild->children[b];
        }
    }

    //previously this was the median in the parent but now we are moving it down to the child on the right
    thischild->entries[0] = entries[a-1];

    //if it isn't a leaf we need to move the last child from the leftchild to the first child postition in thischild
    if(isleaf==false){
    	thischild->children[0] = leftchild->children[leftchild->entrycount];
    }
    //the new median in the parent comes from the last entry of the leftchild
    entries[a-1] = leftchild->entries[leftchild->entrycount-1];

    //added an entry to thischild (on the right) so
    thischild->entrycount++;
    //rotated an entry out of the leftchild so
    leftchild->entrycount--;
}

//rotates a key counterclockwise (moving it up and left)
void bTreeNode::moveLeft(int a){
    bTreeNode *thischild=children[a];
    bTreeNode *rightchild=children[a+1];

    //previous median in parent in moved down into the left child in the last entry position
    thischild->entries[(thischild->entrycount)] = entries[a];

    //first child from the rightchild is not the last child on thischild(leftchild)
    if(thischild->isleaf==false){
        thischild->children[(thischild->entrycount)+1] = rightchild->children[0];
    }
    //first entry from the right child in now the median in the parent
    entries[a] = rightchild->entries[0];

    //we made a void in the first position in the right child so loop moving all of these left
    for (int b=1; b<rightchild->entrycount; ++b){
    	rightchild->entries[b-1] = rightchild->entries[b];
    }
    //if this is not a leaf we did the same thing as above with the children array, moving left
    if (rightchild->isleaf==false){
        for(int b=1; b<=rightchild->entrycount; ++b)
        	rightchild->children[b-1] = rightchild->children[b];
    }

    //rotated an entry into this node so
    thischild->entrycount++;
    //rotated an entry out of this node so
    rightchild->entrycount--;
}

//creates a single child out of two
void bTreeNode::createSingleChild(int a)
{
    bTreeNode *thischild = children[a];
    bTreeNode *leftchild = children[a+1];

    //pull down the median from the parent and put it in the child
    thischild->entries[mindeg-1] = entries[a];

    //copy entries from left child into this child,  our goal is to delete the left child at the end
    for (int i=0; i<leftchild->entrycount; ++i)
        thischild->entries[i+mindeg] = leftchild->entries[i];

    //if this is not a leaf copy children from left child into this child
    if(thischild->isleaf==false){
        for(int i=0; i<=leftchild->entrycount; ++i)
            thischild->children[i+mindeg] = leftchild->children[i];
    }

    //fill the gap in the current node created by taken the median and putting it in the child
    for(int b=a+1; b<entrycount; ++b){
    	entries[b-1] = entries[b];
    }

    //instead of having these two children, this node only now has one combined child
    //overwrite the one child in children and move over the other children
    for(int b=a+2; b<=entrycount; ++b){
        children[b-1] = children[b];
    }

    //new combined child has the combined number of entries plus the one median we added
    thischild->entrycount += leftchild->entrycount+1;

    //we moved the median from this node into the child so
    entrycount--;

    //delete this no longer used leftchild
    delete(leftchild);
    return;
}
